

'use strict'


var app = angular.module('myApp',[])

app.controller('MainController',function($scope) {

	$scope.tasks = []

	var taskData = localStorage['taskLists']
	var lastInsertedId = localStorage['lastInsetId']

	if(taskData !== undefined){
		$scope.tasks = JSON.parse(taskData)
		//$scope.lastId = JSON.parse(lastInsertedId)
	}else{
		localStorage['lastInsetId'] = 1;
	}

	$scope.buttonText = "Save"

	$scope.searchEnter = function (event) {
		if(event.which == 13 && ( $scope.taskName != null && $scope.taskName != '' && $scope.taskName.length != 0 )){
			if($scope.taskId===undefined){
			 	$scope.buttonText = "Saving..."
			 	$scope.addTask()
			}else{
			 	$scope.buttonText = "Updating..."
				$scope.updateTask()
			}
		}
	}
	console.log($scope.tasks)

	$scope.submitTask  = function(){
		if($scope.taskName != null && $scope.taskName != '' && $scope.taskName.length != 0 ){
			if($scope.taskId===undefined){
			 	$scope.buttonText = "Saving..."
			 	$scope.addTask()
			}else{
			 	$scope.buttonText = "Updating..."
				$scope.updateTask()
			}
		}
	}

	$scope.addTask = function () {
		//console.log($scope.taskId)
		$scope.tasks.push({'taskId': localStorage['lastInsetId'], 'taskName':$scope.taskName, 'status':0})
		//console.log($scope.tasks)
		$scope.taskName=''
		$scope.buttonText = "Save "

		localStorage['taskLists'] = JSON.stringify($scope.tasks)
		localStorage['lastInsetId'] = (parseInt(localStorage['lastInsetId'])+1)
		//console.log(localStorage)
	}

	$scope.updateTask = function () {
		var indexes = $scope.currentTaskId($scope.taskId)
		$scope.tasks[indexes].taskName=$scope.taskName
		localStorage['taskLists'] = JSON.stringify($scope.tasks)
		$scope.taskName=''
		$scope.buttonText = "Save "
	}

	// $scope.contentEdit = function(event) {
	// 	event.target.setAttribute('contenteditable', 'true')
	// }

	// $scope.enterAgain = function(msg){
	// 	if(event.which == 13 && ( msg != null && msg != '' && msg.length != 0 )){
	// 		event.target.setAttribute('contenteditable', 'false')
	// 	}
	// }

	$scope.changeStatus = function(id){
		//console.log(id)
		// for (var i = 0; i < $scope.tasks.length; i++) {
		// 	if($scope.tasks[i].taskId===id){
		// 		if($scope.tasks[i].status===1){
		// 			$scope.tasks[i].status=0
		// 		}else{
		// 			$scope.tasks[i].status=1
		// 		}
		// 	}
		// }

		$scope.taskId = id
		var indexes = $scope.currentTaskId($scope.taskId)
		if($scope.tasks[indexes].status===1){
			$scope.tasks[indexes].status=0
		}else{
			$scope.tasks[indexes].status=1
		}

		localStorage['taskLists'] = JSON.stringify($scope.tasks)
	}
	
	$scope.editTask = function(id){
		//console.log(id)
		//$scope.taskName = $scope.tasks[id].taskName
		$scope.taskId = id
		var indexes = $scope.currentTaskId($scope.taskId)
		console.log(indexes)
		$scope.taskName = $scope.tasks[indexes].taskName
		$scope.buttonText = "Update"
	}

	$scope.deleteTask = function(id){
		$scope.taskId = id
		var indexes = $scope.currentTaskId($scope.taskId)
		$scope.tasks.splice(indexes,1)
		localStorage['taskLists'] = JSON.stringify($scope.tasks)
	}


	$scope.currentTaskId = function(id){
		var indexes = $scope.tasks.map(function(obj, index) {
		    if(obj.taskId == id) {
		        return index;
		    }
		}).filter(isFinite)

		return indexes;
	}

})

